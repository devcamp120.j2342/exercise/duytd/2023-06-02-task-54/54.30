package com.devcamp.s5430.randomnumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomnumberApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandomnumberApplication.class, args);

		RandomNumber randomNumber = new RandomNumber();

		System.out.println(randomNumber.randomNumberDouble());

		System.out.println(randomNumber.randomNumberInt());
	}

}
