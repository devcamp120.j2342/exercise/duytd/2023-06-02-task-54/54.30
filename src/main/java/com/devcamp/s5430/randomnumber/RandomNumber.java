package com.devcamp.s5430.randomnumber;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class RandomNumber {
    @GetMapping("random-double")
    public String randomNumberDouble() {
        double randomDoublenum = 0;
        for (int i = 0; i < 100; i++) {
            randomDoublenum = Math.random() * 100;
        }
        return "Random value is double form 1 to 100: " + randomDoublenum;
    }

    @GetMapping("random-int")
    public String randomNumberInt() {
        int randomIntNum = 1 + (int) (Math.random() * ((10 - 1) + 1));
        return "Random value is double form 1 to 10: " + randomIntNum;
    }

}
